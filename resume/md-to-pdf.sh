#!/bin/sh

lang=${1:?"Pass either en or de as the language"}

case "$lang" in
 en) pdfName="Resume" ;;
 de) pdfName="Lebenslauf" ;;
esac

curl \
  --url https://md-to-pdf.fly.dev \
  --data-urlencode "markdown=$(cat "resume/resume-$lang.md")" \
  --data-urlencode "css=$(cat resume/styles.css)" \
  --output "public/BenediktFranke-$pdfName.pdf"
