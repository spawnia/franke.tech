# Benedikt Franke

## Software Developer

[benedikt@franke.tech](mailto:benedikt@franke.tech) +49 174 1999723

I bring order to chaos.

### Skills

#### DevOps

Streamline development processes and continuously deliver software through automation and containerization.

#### Web Development

Proficiently design and create modern web applications that are both reliable and highly scalable.

#### Project Direction

Able to engineer and manage projects independently or in a team and get things done the right way.

### Technical

|            |                        |       |
| ---------- | ---------------------- | ----- |
| Docker     | Kubernetes             | Linux |
| Git        | Infrastructure as code | CI/CD |
| TypeScript | PHP                    | SQL   |
| GraphQL    | HTML                   | CSS   |

### Work

MLL Münchner Leukämielabor `01/2018-Current`

_Software Developer_

I am responsible for developing and modernizing the internal IT infrastructure and application landscape.
Under my lead, the team _Platform Development_ provides the development platform for the other software teams.
I closely collaborate with users and plan, develop and maintain our systems.
By automating tests, configuration, and deployment, I ensure quality and increase development velocity.

#### twofour digitale Agentur GmbH `01/2017-12/2017`

_Software Developer_

I started an internship at twofour as part of my studies and was later employed full time.
My Tasks were centered around PHP based CMS and shop frameworks. Through test-driven development
in agile Scrum teams we rapidly delivered challenging software solutions to our customers.
My responsibilities included project setup, backend and frontend development and optimization of our
deployment pipeline.

#### Freudenberg Sealing Technologies `06/2016-07/2016`

_Software Developer_

Together with two colleagues we developed a web application to automate the food
ordering system of the company's cafeteria. After assessing the requirements of the project,
I planned and set up the database and configured the application server. I automated
the process of exporting the collected data and sending it to the catering provider.

#### Media Markt `03/2013-02/2014`

_Computer Salesman_

My areas of work included advising customers, providing technical service and
placement of goods in the store. I was able to gather valuable experience
through constant exposure to computers and refined my communication skills.

### Education

#### FH Kufstein - Web Business & Technology `09/2014-07/2017`

_Bachelor of Science in Engineering_ `graduated "with excellent success"`

This bachelor program deals with the basics of technological and economical knowledge
required to design and create web-based information systems. The focus is set on imparting
a fully qualified understanding of web-based business models, applications and web technologies.
I learned principles of databases, programming and software engineering, and applied them in
two web application projects for the Kufstein based Freudenberg GmbH.

#### University of Regensburg `10/2012-02/2013`

_General Chemistry, no graduation_

#### Annette-Kolb-Gymnasium Traunstein `09/2004-06/2012`

_High School, Final Grade: 2.2_

### Languages

**German** Native speaker

**English** Fluent in written and spoken

**Spanish** Basic knowledge
