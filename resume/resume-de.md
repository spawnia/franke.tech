# Benedikt Franke

## Softwareentwickler

[benedikt@franke.tech](mailto:benedikt@franke.tech) +49 174 1999723

Ich bringe Ordnung ins Chaos.

### Skills

#### DevOps

Optimierung der Entwicklung und kontinuierliche Auslieferung von Software durch Automatisierung und Container.

#### Webentwicklung

Konzeption und Umsetzung moderner Web-Anwendungen, die sowohl zuverlässig als auch skalierbar sind.

#### Projektleitung

Planung und Durchführung von komplexen Softwareprojekten, sowohl selbstständig als auch im Team.

### Technologien

|            |                        |       |
| ---------- | ---------------------- | ----- |
| Docker     | Kubernetes             | Linux |
| Git        | Infrastructure as code | CI/CD |
| TypeScript | PHP                    | SQL   |
| GraphQL    | HTML                   | CSS   |

### Arbeit

#### MLL Münchner Leukämielabor `01/2018-Aktuell`

_Softwareentwickler_

Ich bin verantwortlich für die Weiterentwicklung und Modernisierung der internen IT-Infrastruktur und verwendeten Anwendungen.
Unter meiner Leitung stellt das Team _Plattformentwicklung_ die Entwicklungsplattform für die Arbeit der anderen Softwareteams bereit.
Dabei arbeite ich eng mit den Nutzern zusammen und plane, entwickle und verwalte unsere Systeme.
Durch die Automatisierung von Softwaretests, Konfiguration, und Deployment, stelle ich die Qualität der Anwendung sicher und beschleunige die Entwicklung.

#### twofour digitale Agentur GmbH `01/2017-12/2017`

_Softwareentwickler_

Als Teil meines Studiums begann ich bei twofour ein Praktikum, welches anschließend in eine Festanstellung überging.
Dort beschäftigte ich mich hauptsächlich mit PHP basierten CMS und Shop-Frameworks.
Durch den Einsatz von testgetriebener Entwicklung im agilen Scrum-Umfeld lieferten wir herausfordernde Softwarelösungen an unsere Kunden.  
Meine Aufgaben beinhalteten das Setup von Projekten, Entwicklung in Backend und Frontend sowie die Optimierung unserer Deploymentprozesse.

#### Freudenberg Sealing Technologies `06/2016-07/2016`

_Softwareentwickler_

Gemeinsam mit zwei Kommilitonen entwickelten wir eine Webanwendung für die automatisierte Bestellung
von Essen in der betriebsinternen Kantine. Nachdem wir die Anforderungen für das Projekt erhoben hatten,
plante und erstellte ich das Datenbankschema und konfigurierte den Anwendungsserver. Ich automatisierte
den Export der eingegebenen Daten und deren Übermittlung an den Lieferdienst.

#### Media Markt `03/2013-02/2014`

_Verkäufer Computerabteilung_

Meine Aufgaben bestanden in der Beratung von Kunden, Bereitstellung technischer Serviceleistung
sowie der Präsentation von Waren im Geschäft. Ich konnte wertvolle Erfahrungen sammeln durch
die Arbeit mit technischen Geräten und den persönlichen Kontakt zu Kunden.

### Ausbildung

#### FH Kufstein - Web Business & Technology `09/2014-07/2017`

_Bachelor of Science in Engineering_ `Abschluss "mit ausgezeichnetem Erfolg"`

Inhalt dieses Bachelorstudiums sind die wirtschaftlichen und technologischen Grundlagen webbasierter Informationssysteme.
Der Fokus liegt auf einem ganzheitlichen Verständnis webbasierter Geschäftsmodelle sowie Anwendungen und Technologien des Web.
Hierbei lernte ich die Prinzipien von Datenbanken, Programmierung und Software-Engineering
und konnte diese in zwei Praxisprojekten in Kooperation mit der Freudenberg GmbH in Kufstein umsetzen.

#### Universität Regensburg `10/2012-02/2013`

_Allgemeine Chemie, ohne Abschluss_

#### Annette-Kolb-Gymnasium Traunstein `09/2004-06/2012`

_Abitur, Abschlussnote 2,2_

### Sprachen

**Deutsch** Muttersprache

**Englisch** Fließend in Wort und Schrift

**Spanisch** Grundkenntnisse
