.PHONY: help
help: ## Displays this list of targets with descriptions
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: setup
setup: up node_modules resume-de resume-en ## Setup dev environment

.PHONY: up
up: ## Bring up the container stack
	docker-compose up --detach

.PHONY: shell
shell: node_modules ## Enter an interactive shell
	docker-compose exec cypress bash

.PHONY: prettify
prettify: node_modules ## Prettify code
	docker-compose exec cypress yarn run prettier --write .

.PHONY: cypress
cypress: node_modules ## Prettify code
	docker-compose exec cypress yarn run cypress run

resume-de: ## Make the german resume pdf
	docker-compose exec curl resume/md-to-pdf.sh de

resume-en: ## Make the english resume pdf
	docker-compose exec curl resume/md-to-pdf.sh en

node_modules:
	docker-compose exec cypress yarn
