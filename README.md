# franke.tech

The source code for my personal homepage at https://franke.tech

I decided to open source this project, maybe it will be useful to someone else.
This serves both as a description of my progress, and a reminder to myself of what I did here.

## Design

The page has a sleek, minimalist look.
It is functional, rather than fancy - it serves the simple purpose of pointing to a bunch of information about me.

## Resume generation

My resume is also contained in this project, written as plain markdown text files
which are rendered to PDF by [md-to-pdf](https://github.com/spawnia/md-to-pdf).

## Deployment

Gitlab CI is used to compile the resume and build the files for the static site.
Those are then deployed via [Vercel](https://vercel.com).

## Development

All development tasks are defined in the [Makefile](Makefile) and can be listed with:

    make help
