describe('franke.tech', () => {
  it('Shows the default index page', () => {
    cy.visit('/')
    cy.contains('Benedikt Franke')
    cy.contains('I bring order to chaos.')
  })

  it('Can download the resume', () => {
    cy.request('/BenediktFranke-Resume.pdf').then((response) => {
      expect(response.headers).to.have.property(
        'content-type',
        'application/pdf'
      )
    })
  })
})
